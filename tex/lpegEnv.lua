lpeg = require "lpeg"

word = (lpeg.R("az") + lpeg.R("AZ"))^1

function lazy(p,q)
  q=q or lpeg.P(0)
  return lpeg.P{ q + p * lpeg.V(1) }
end
function greedy(p,q)
  q=q or lpeg.P(0)
  return lpeg.P{ p * lpeg.V(1) + q }
end

function maF(corde)
  return nil
end
tab = {verba=maF}
function verifier(s,i,capture)
  res = tab[capture]
  return res and true, res
end
theEnvi = lpeg.Cb("envName")
beg = lpeg.P
{
 [[\begin{]] * lpeg.Cg(word,"envName") * theEnvi * lpeg.Cmt(theEnvi, verifier) * [[}]]
}

corde = [[blabla \begin{beg} \begin{verba} interieur]]

pat = lazy(1,lpeg.Cp() * #beg) * beg * lpeg.Cp() 
--print(corde:sub(1,pat:match(corde)-1))

--tab["beg"]=maF
d,e,f,nd = pat:match(corde)
if d then
  debut=corde:sub(1,d-1)
  body=corde:sub(nd)
  print(d,e,f,nd)
  print(debut, e ,body)
  --print(fin)
  --print(e)
else
  print("noMatch")
end

--tab["beg"]=ninil
--print(corde:sub(1,pat:match(corde)-1))
