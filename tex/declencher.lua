lpeg = require"lpeg"

word = (lpeg.R("az") + lpeg.R("AZ"))^1
function lazy(p,q)
  q=q or lpeg.P(0)
  return lpeg.P{ q + p * lpeg.V(1) }
end
function maF(corde)
  cedric=corde
  --tex.print(corde)
end
--maF=tex.print
function verifier(s,i,capture)
  res = tab[capture]
  return res and true, res
end
theEnvi = lpeg.Cb("envName")
beg = lpeg.P
{
 [[\begin{]] * lpeg.Cg(word,"envName") * theEnvi * lpeg.Cmt(theEnvi, verifier) * [[}]]
}
patDeb = lazy(1,lpeg.Cp() * #beg) * beg * lpeg.Cp() 

scanning=nil
--res=""
--res={}
--body=""

--[[
function split(corde,sep)
  pat = lpeg.Ct((lpeg.C(lazy(1,#sep)))^0)
  return pat:match(corde)
end
--]]
--[[
function split(corde,sep)
  sep = lpeg.P(sep) + lpeg.P(-1)
  --pat = lpeg.Ct((lpeg.C(1*lazy(1,#sep))*sep)^0)
  pat = lpeg.Ct((lpeg.C(1*lazy(1,sep)))^0)
  return pat:match(corde)
end

function split(corde,sep)
  sep = lpeg.P(sep) + lpeg.P(-1)
  --pat = lpeg.Ct((lpeg.C(1*lazy(1,#sep))*sep)^0)
  pat = lpeg.Ct(lpeg.C(1*lazy(1,sep))^0)
  return pat:match(corde)
end

function splitLines(corde)
  return split(corde,"\n")
end
--]]

function split(corde,sep)
  sep = lpeg.P(sep) 
  pat = lpeg.Ct((lpeg.C(lazy(1,#sep))*sep + lpeg.C(lpeg.P(1)^1) * -1)^0)
  return pat:match(corde)
end
function splitLines(corde)
  return split(corde,"\n")
end

function readbuf(ligne)
  if scanning then
    d,nd = patEnd:match(ligne)
    if d then
      after=ligne:sub(nd)
      body = body .. ligne:sub(1,d-1) .. "\n"
      scanning=false
      pat = lpeg.P(" ")^0 * lpeg.Cc("%") * -1 -- return commented newline if no text 
      after = pat:match(after) or after
      return [[\directlua{calledFonction(body)}]] .. after 
    else -- no match for end{envi}
      body = body .. ligne .. "\n"
      return "%"
    end
  else -- not scanning
    d,envi,calledFonction,nd = patDeb:match(ligne)
    if d then
      print(d,envi,calledFonction,nd)
      print(d,envi,calledFonction,nd)
      print(d,envi,calledFonction,nd)
      print(d,envi,calledFonction,nd)
      avant, after = ligne:sub(1,d-1), ligne:sub(nd)
      body, scanning = "", true
      theEnd = lpeg.P([[\end{]] .. envi .. [[}]])
      patEnd = lazy(1,lpeg.Cp() * theEnd) * lpeg.Cp() 
      return avant .. readbuf(after)
    else -- no match for begin{envi}
      return nil
    end
  end
end

--tab={}
tab = {verba=maF}
function faireEnvi(envi,fonction)
  --loadstring("tab['" .. envi .. "']=" .. fonction)()
  tab[envi] = fonction
end
