do 
  lpeg = require "lpeg"
  lpeg.locale(lpeg)
  space = lpeg.space^0
  anyChar = lpeg.P(1)
  word = (lpeg.R("az") + lpeg.R("AZ"))^1
  eos = lpeg.P(-1)

  function lazy(p,q)
    q=q or lpeg.P(0)
    return lpeg.P{ q + p * lpeg.V(1) }
  end
  function verifyMatch(mot)
    return function(s,i,match)
      return match==mot
    end
  end

  function beginEnv(verify)
    return [[\begin{]] * lpeg.Cmt(lpeg.C(word), verify) * "}"
  end
  function endEnv(verify)
    return [[\end{]] * lpeg.Cmt(lpeg.C(word), verify) * "}" 
  end

  beginEnumerate = beginEnv(verifyMatch("enumerate"))
  endEnumerate = endEnv(verifyMatch("enumerate"))
  itemKey = lpeg.P[[\item]]
  repKey = lpeg.P[[\rep ]]

  function effacerReponses(body)
    endRep = space * ( endEnumerate + itemKey + eos )
    rep = space * repKey * lazy(1,#endRep)
    matcherRep = (1*lazy(1,rep/"" + -1))^0
    return lpeg.Cs(matcherRep):match(body)
  end

  questionWithAnswer = itemKey * space * lpeg.C(lazy(1 - itemKey, #(space * repKey)))
  function answersLitem(body)
    replace = [[\litem{%1}]]
    matcherQwA = (1*lazy(1,questionWithAnswer/replace + -1))^0
    return lpeg.Cs(matcherQwA):match(body)
  end

  function bracedBy(delDeb,delEnd,char)
    char = char or anyChar
    return delDeb  * lpeg.C(lazy(char,#delEnd)) * delEnd
  end

  noReturn=lpeg.P(1)-"\n"
  opB,clB = lpeg.P"[",lpeg.P"]"
  dP = lpeg.P":"

  function titreLabelBody(rawBody)
    local titre = bracedBy(opB,clB,noReturn)
    local label = bracedBy(dP,dP,noReturn)
    local matcherTitre = lazy(noReturn,lpeg.Cg(titre,"titre"))^-1
    local matcherLabel = lazy(noReturn,lpeg.Cg(label,"label"))^-1
    local matcherBody = space * lpeg.Cg(lazy(1,#(space * eos)),"body")
    local pat = matcherTitre * matcherLabel * matcherBody
    return lpeg.Ct(pat):match(rawBody)
  end

  listeExos={}
  aliases={}
  function traiterBody(rawBody)
    local c = titreLabelBody(rawBody)
    table.insert(listeExos,c)
    local b,t,l = c.body,c.titre,c.label
    if l then
      aliases[l]=#listeExos
    end
  end

  function enonceExercice(num)
    local c = listeExos[num]
    local b,t,l = c.body,c.titre,c.label
    local premiereLigne = [[\begin{exercice}]] .. (t and "[".. t .. "]" or "")
    local corps = effacerReponses(b)
    local derniereLigne = [[\end{exercice}]]
    return premiereLigne .. "\n" .. corps .. "\n" .. derniereLigne
  end
  function enonceSolution(num)
    local c = listeExos[num]
    local b,t,l = c.body,c.titre,c.label
    local premiereLigne = [[\begin{solution}]] .. (t and "[".. t .. "]" or "")
    local corps = answersLitem(b) 
    local derniereLigne = [[\end{solution}]]
    return premiereLigne .. "\n" .. corps .. "\n" .. derniereLigne
  end

end
