lpeg = require "lpeg"

luaVerbatimEnvironments={"luaVerbatim"}
openBracket=lpeg.P"{"
closBracket=lpeg.P"}"
openDelim=lpeg.S"{["
closDelim=lpeg.S"}]"
lettre = lpeg.R("az") + lpeg.R("AZ")
valid = lpeg.P(1) - "\n"
white = lpeg.S(" \t\r\n")^1
stopScan=closDelim * white
controlWord=lettre^1
argument= openDelim * (valid - stopScan)^1 * stopScan 

luaVeLPE = lpeg.P(luaVerbatimEnvironments[1])
patBeg = lpeg.P([[\begin]]) * openBracket * luaVeLPE * closBracket
patEnd = lpeg.P([[\end]]) * openBracket * luaVeLPE * closBracket

openEnvi= lpeg.C(patBeg * argument^-1)

corde = [[
\begin{luaVerbatim}
]]

pat = openDelim
pat = argument
pat = openEnvi
print(pat:match(corde))
--print(openDelim:match(corde))
