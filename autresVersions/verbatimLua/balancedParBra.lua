--see also balancedPar for a working version 
lpeg = require "lpeg"
yaml = require "yaml"
b = lpeg.P{ "(" * ((1 - lpeg.S"()") + lpeg.V(1))^0 * ")" * -1 }
op = lpeg.S"[("
cl = lpeg.S"])"
corr ={["]"] = "[", [")"] = "(", ["["] = "]", ["("] = ")", }
function equalMatches(s, i, a, b, c)
  d= c or b 
  --table.insert(a,1,b) table.insert(a,corr[b]) -- embrassées
  table.insert(a,1,b..corr[b]) -- début-fin
  return (b == c), a
end

char=(1 - op - cl)
b = lpeg.P{
  "bal",
  bal=lpeg.Ct((char + lpeg.V("balPar"))^0),
  --balPar = lpeg.Cg(op,"opening") *  lpeg.V("bal") * lpeg.Cmt(cl / corr * lpeg.Cb("opening"),equalMatches) ;
  balPar = lpeg.Cg(op,"cle") * lpeg.Cmt(lpeg.V("bal") * lpeg.Cb("cle") * (cl/corr) , equalMatches) ;
}

---[[
s=[=[(((()[]()())()))]=]
--]]
--[[
s=[=[
()()
]=]
--]]

white=lpeg.S(" \n")^0
--b=b*white*-1
function tilEnd(p)
  return p*white*-1
end
b=tilEnd(b)
print(b:match(s) and yaml.dump(b:match(s)[1]) or "NOMATCH")
