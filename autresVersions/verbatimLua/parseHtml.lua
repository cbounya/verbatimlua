lpeg = require "lpeg"

lettre = lpeg.R("az") + lpeg.R("AZ")
tab = lettre^1
op = lpeg.P"<"
cl = lpeg.P">"
text= (lpeg.P(1) - op - cl - [[/]])^1
opTag = op * lpeg.Cg(text,"oper") * cl
clTag = op * lpeg.P([[/]]) * lpeg.Cb("oper") * cl
simple = opTag * text * clTag

--[[
html = lpeg.P{
  "valid";
  valid = text * tag * text,
}
--]]

maCorde = [[
<a>i</a>
]]

pat = simple
print(pat:match(maCorde))

print(pat == "a")
print(type(pat))

