local re = require 're'

local list_parser = re.compile [[
   list <- ( singleint_or_range ( ',' singleint_or_range ) * ) -> {}
   singleint_or_range <- range / singleint
   singleint <- { int } -> {}
   range <- ( { int } '-' { int } ) -> {}
   int <- %d+
]]
