function fromTo(n,m)
  local i = n-1 
  return function()
    i = i + 1
    return i < m+1 and i or nil
  end
end
function fromToBy(n,m,by)
  local by = by or 1
  local i = n - by
  return function()
    i = i + by
    return i < m+1 and i or nil
  end
end


for k=2,10 do 
  --print(k)
end
for k in fromTo(2,5) do 
  --print(k)
end

by = 4
for k in fromToBy(2,9,2) do 
  print(k)
end
