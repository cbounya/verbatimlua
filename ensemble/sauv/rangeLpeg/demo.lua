dofile("demo.lua")

local function parse_list(list_string)
   local t = list_parser:match(list_string)
   -- further processing to remove overlaps, duplicates, sort into ascending order, etc
   return t
end
