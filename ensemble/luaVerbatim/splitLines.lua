lpeg = require "lpeg"

function lazy(p,q)
  q=q or lpeg.P(0)
  return lpeg.P{ q + p * lpeg.V(1) }
end

function split(corde,sep)
  sep = lpeg.P(sep) 
  -- sep = lpeg.P(sep) + lpeg.P(-1)
  --pat = lpeg.Ct((lpeg.C(1*lazy(1,#sep))*sep)^0)
  pat = lpeg.Ct((lpeg.C(lazy(1,#sep))*sep + lpeg.C(lpeg.P(1)^1) * -1)^0)
  -- pat = lpeg.Ct((lpeg.C(lazy(1,#sep))*sep )^0)
  --pat = lpeg.Ct((lpeg.C(lazy(1,sep)))^0)
  return pat:match(corde)
end
function splitLines(corde)
  return split(corde,"\n")
end

corde = [[
premier
deuxieme

troisieme
]]

corde = [[
abcde
fghi

jkl]]


--split(corde,"|")
--[[
for _,v in pairs(split(corde,"\n")) do
  print(v .. "|")
end
--]]

a = lazy(1,-1)

function magiclines(s)
  if s:sub(-1)~="\n" 
  then 
    return (s.."\n"):gmatch("(.-)\n")
  else
    return s:gmatch("(.-)\n")
  end
end
function magiclines(s)
  if s:sub(-1)~="\n" 
  then 
    return (s.."\n"):gmatch("(.-)\n")
  else
    return s:gmatch("(.-)\n")
  end
end

linesPat = "([^\r\n]*)[\r\n]?"

function iterer(corde,fonction)
  for ligne in magiclines(corde) do fonction(ligne) end
end

function splitMagic(corde)
  --tex.print([[\begin{verbatim}]])
  iterer(corde,tex.print)
  --tex.print([[\end{verbatim}]])
end

--[[
function iterer(corde,fonction)
  for ligne in corde:gmatch(linesPat) do fonction(ligne) end
end
--]]


--print(lpeg.C(a):match(corde))
--print(split(corde,"\n")[1])

--[[
tab={"abcde", "fghi", "", "jkl"}
res = split(corde,"\n")
print(type(tab),type(res))
print(#tab,#res)
for i=1,4 do 
print(tab[i] == res[i])
end
--]]
