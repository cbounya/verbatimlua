do 
  local lpeg = require "lpeg"
  -- local yaml = require "yaml"

  local white = lpeg.P" "^0
  local digit = lpeg.R("09")
  local lower = lpeg.R("az")
  local upper = lpeg.R("AZ")

  local int = digit^1
  local word = (lower + upper + digit)^0

  local comma = lpeg.P","
  local delim = lpeg.P":"

  aliases = aliases or {}
  function checkAlias(mot)
    local a = aliases[mot]
    if a then 
      return a.numSav
    else
      texio.write_nl(warningMsg .. "alias :" .. mot .. ": not found")
      return false
    end
  end

  local listParser = lpeg.P{
    "list",
    list = lpeg.Ct(lpeg.V"token" * (comma * lpeg.V"token")^0),
    token = lpeg.V("range") + lpeg.V("int") + lpeg.V("key"),
    key = word/checkAlias,
    int = int/tonumber,
    range = lpeg.Ct(lpeg.V"int" * delim * lpeg.V"int" * (delim * lpeg.V"int")^-1),
  }

  function fromToBy(n,m,by)
    local by = by or 1
    local i = n - by
    return function()
      i = i + by
      return i < m+1 and i or nil
    end
  end

  function processListe(body,f)
    --local f = f or print
    local f = f or function() end
    local operations =
    {
      table = function(v)
        --for i=v[1],v[2] do 
        for i in fromToBy(v[1],v[2],v[3]) do 
          f(i)
        end
      end;
      number = f;
      --string = f;
    }
    local t = listParser:match(body)
    for _,v in ipairs(t) do
      if v then
        operations[type(v)](v)
      end
    end
  end
end
