warningMsg = "luaTeXo Warning: "
do 
  ---[[ lpeg declarations
        local lpeg = require "lpeg"
        lpeg.locale(lpeg)
        local space = lpeg.space^0
        local anyChar = lpeg.P(1)
        local word = (lpeg.R("az") + lpeg.R("AZ"))^1
        local eos = lpeg.P(-1)
        local comment = [[%]] * lazy(1,"\n")
        local noReturn=lpeg.P(1)-"\n"
  --]]

  ---[[ general purpose lpeg functions
        function lazy(p,q)
          q=q or lpeg.P(0)
          return lpeg.P{ q + p * lpeg.V(1) }
        end

        function verifyMatch(mot)
          return  function(s,i,match)
                    return match==mot
                  end
        end

        function checkEnv(beginOrEnd, verify)
          return  lpeg.P[[\]] * beginOrEnd  * lpeg.Cmt(lpeg.C(word), verify) * "}" 
        end

        function bracedBy(delDeb,delEnd,char)
          local char = char or anyChar
          return delDeb  * lpeg.C(lazy(char,#delEnd)) * delEnd
        end
  --]]

  ---[[ syntax elements
        local opB,clB = lpeg.P"[",lpeg.P"]"
        local dP = lpeg.P":"
        local beginEnumerate = checkEnv("begin", verifyMatch("enumerate"))
        local endEnumerate = checkEnv("end", verifyMatch("enumerate"))
        local itemKey = lpeg.P[[\item]]
        local repKey = lpeg.P[[\rep ]]
  --]]

  local tousExos  = tousExos  or {1,0} -- will contain the table {1,numLastExo}
  listeExos = listeExos or {}
  aliases   = aliases   or {}
  aliases.tousExos = tousExos

  ---[==[ process les exercices enregistrés
        function processBody(rawBody)
          local c = titreLabelBody(rawBody)
          table.insert(listeExos,c)
          tousExos[2] = tousExos[2]+1
          c.numSav=tousExos[2]
          local b,t,l = c.body,c.titre,c.label
          if l then
            --aliases[l]=#listeExos
            aliases[l]=c
          end
        end

        function titreLabelBody(rawBody)
          local titre, label = bracedBy(opB,clB,noReturn), bracedBy(dP,dP,noReturn)
          local matchTitre = lazy(noReturn,lpeg.Cg(titre,"titre"))^-1
          local matchLabel = lazy(noReturn,lpeg.Cg(label,"label"))^-1
          local matchBody = space * lpeg.Cg(lazy(1,#(space * eos)),"body")

          local pattern = matchTitre * matchLabel * matchBody
          return lpeg.Ct(pattern):match(rawBody)
        end

        function effacerReponses(body)
          local endRep = space * ( beginEnumerate + endEnumerate + itemKey + eos )
          local rep = space * repKey * lazy(1,#endRep)
          local matchRep = (1*lazy(1,rep/"" + -1))^0
          return lpeg.Cs(matchRep):match(body)
        end

        function answersLitem(body)
          local endingAnswer = space * comment^-1 * space * repKey
          local questionWithAnswer = itemKey * space * lpeg.C(lazy(1 - itemKey, #endingAnswer)) 
          local replaceBy = [[\litem{%1}]]
          local matchQwA = (1*lazy(1,questionWithAnswer/replaceBy + -1))^0
          return lpeg.Cs(matchQwA):match(body)
        end
  --]==]

  ---[==[ fonctions d'affichage
          function enonceExercice(c)
            --local c = listeExos[num]
            local b,t,l = c.body,c.titre,c.label
            c.numExo = ""
            local premiereLigne = [[\begin{exercice}]] .. (t and "[".. t .. "]" or "")
            local corps = effacerReponses(b)
            local derniereLigne = [[\end{exercice}]]
            return premiereLigne .. "\n" .. corps .. "\n" .. derniereLigne
          end
          function enonceSolution(c)
            --local c = listeExos[num]
            local b,t,l = c.body,c.titre,c.label
            local premiereLigne = [[\begin{solution}]] .. (t and "[".. t .. "]" or "")
            local corps = answersLitem(b) 
            local derniereLigne = [[\end{solution}]]
            return premiereLigne .. "\n" .. corps .. "\n" .. derniereLigne
          end
          function traduire(label)
            local c = aliases[label] 
            if c then
              return c.numSav
            end
          end
  --]==]

  require("liste.lua")
end
