do 
  local lpeg = require"lpeg"

  local word = (lpeg.R("az") + lpeg.R("AZ"))^1
  function lazy(p,q)
    q=q or lpeg.P(0)
    return lpeg.P{ q + p * lpeg.V(1) }
  end
  function verifier(s,i,capture)
    res = tab[capture]
    return res and true, res
  end
  local anyChar = lpeg.P(1)
  local theEnvi = lpeg.Cb("envName")
  local beg = lpeg.P
  {
    [[\begin{]] * lpeg.Cg(word,"envName") * theEnvi * lpeg.Cmt(theEnvi, verifier) * [[}]]
  }
  local patDeb = lazy(anyChar-[[%]],lpeg.Cp() * beg) * lpeg.Cp() 

  -- captures commented newline if no text, captures subject otherwise
  local commentIfWhite = lpeg.P(" ")^0 * ([[%]] * anyChar^0 )^-1 * lpeg.Cc("%") * -1 + lpeg.C(lpeg.P(1)^0) 

  --[[
  function swapCallbacks(f,g)
    luatexbase.remove_from_callback('process_input_buffer', f, 'cedric')
    luatexbase.add_to_callback('process_input_buffer', g, 'cedric')
  end
  --]]

  tab={}
  tab = {verba= function (corde) cedric=corde end}
  function faireEnvi(envi,fonction)
    --loadstring("tab['" .. envi .. "']=" .. fonction)()
    tab[envi] = fonction
  end

  function recordVerba(ligne)
    local d,nd = patEnd:match(ligne)
    if not d then
      body = body .. ligne .. "\n"
      return "%"
    else -- no match for end{envi}
      local insideEnvi, afterEnvi = ligne:sub(1,d-1), ligne:sub(nd)
      body = body .. insideEnvi .. "\n"
      --swapCallbacks(recordVerba,waitForVerba)
      ---[[
      luatexbase.remove_from_callback('process_input_buffer', 'recordVerba')
      luatexbase.add_to_callback('process_input_buffer', waitForVerba, 'waitForVerba')
      --]]
      return [[\directlua{calledFonction(body)}]] .. commentIfWhite:match(afterEnvi) 
    end
  end

  function waitForVerba(ligne)
    local d,envi,calledFonction,nd = patDeb:match(ligne)
    if not d then
      return nil
    else -- no match for begin{envi}
      --print(d,envi,calledFonction,nd)
      local beforeEnvi, insideEnvi = ligne:sub(1,d-1), ligne:sub(nd)
      body = ""
      patEnd = lazy(anyChar,lpeg.Cp() * ([[\end{]] .. envi .. [[}]])) * lpeg.Cp() 
      --swapCallbacks(waitForVerba,recordVerba)
      ---[[
      luatexbase.remove_from_callback('process_input_buffer', 'waitForVerba')
      luatexbase.add_to_callback('process_input_buffer', waitForVerba, 'recordVerba')
      --]]
      return beforeEnvi .. recordVerba(insideEnvi)
    end
  end

  local scanning=false
  function readbuf(ligne)
    --scanning=scanning or false
    if not scanning then
      local d,envi,calledFonction,nd = patDeb:match(ligne)
      if not d then
        return nil
      else -- no match for begin{envi}
        --print(d,envi,calledFonction,nd)
        local beforeEnvi, insideEnvi = ligne:sub(1,d-1), ligne:sub(nd)
        body, scanning = "", true
        patEnd = lazy(anyChar,lpeg.Cp() * ([[\end{]] .. envi .. [[}]])) * lpeg.Cp() 
        return beforeEnvi .. readbuf(insideEnvi)
      end
    else -- not scanning
      local d,nd = patEnd:match(ligne)
      if not d then
        body = body .. ligne .. "\n"
        return "%"
      else -- no match for end{envi}
        local insideEnvi, afterEnvi = ligne:sub(1,d-1), ligne:sub(nd)
        body, scanning = body .. insideEnvi .. "\n", false
        return [[\directlua{calledFonction(body)}]] .. commentIfWhite:match(afterEnvi) 
      end
    end
  end

  --
end

do
  function split(corde,sep)
    local sep = lpeg.P(sep) 
    local pat = lpeg.Ct((lpeg.C(lazy(anyChar,#sep)) * sep + lpeg.C(anyChar^1) * -1)^0)
    return pat:match(corde)
  end

  function splitLines(corde) 
    return split(corde,"\n") 
  end
end
