lpeg = require "lpeg"

function lazy(p,q)
  q=q or lpeg.P(0)
  return lpeg.P{ q + p * lpeg.V(1) }
end

lpeg.P{
  "mot",
  lettre = lpeg.R("az"),
  --mot = lpeg.V("lettre")^1,
  mot = lazy(1,lpeg.V("lettre")^1),
}
