lpeg = require "lpeg"

shortcuts ={}

function addShortcut(titre, table, cle, force)
  local cle = cle or #table
  local force = force or false
  if force or not isShortcut(titre) then
    shortcuts[titre] = {table = table, cle = cle}
  end
end

function isShortcut(titre)
  return shortcuts[titre] and true
end

function accessShortcut(titre)
  local a = shortcuts[titre]
  return a.table[a.cle]
end

tableau = { "lkj" ,"jkl" ,"jlkasdf" ,"fds" ,"fdskl" ,"iop" }


addShortcut("newton",tableau, 3)
print(accessShortcut("newton"))
print(isShortcut("newton"), isShortcut("euler"))
