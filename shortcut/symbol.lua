lpeg = require "lpeg"

texte=[[
:monSymbole:
blablabla
]]

letter = lpeg.R("az")
Letter = lpeg.R("AZ")
digit = lpeg.R("09")
word = (letter + Letter + digit)^1

symbol = lpeg.P {[[:]] * word * [[:]]}

function lazy(p,q)
  q=q or lpeg.P(0)
  return lpeg.P{ q + p * lpeg.V(1) }
end

anyChar = lpeg.P(1)
matcherSymbol = lazy(anyChar , lpeg.C(symbol))

print(matcherSymbol:match(texte))
print(symbol:match(texte))
