lpeg = require "lpeg"
---[[
locale = lpeg.locale()
--[[
print(type(locale))
for k,v in pairs(locale) do 
  print(k,v)
end
--]]
--]]
--[[
a =lpeg.locale(lpeg)
print(lpeg.lower)
--]]

local allchar = {}
for i=0,255 do allchar[i + 1] = i end
allchar = string.char(unpack(allchar))
assert(#allchar == 256)

--allchar = allchar .. [[éèà
specialFrench = lpeg.S[[âàäçéèêëîïôùûüœ]] + lpeg.S[[ÂÀÄÇÉÈÊËÎÏÔÙÛÜŒ]]
allchar = allchar .. [[âàäçéèêëîïôùûüœ]] .. [[ÂÀÄÇÉÈÊËÎÏÔÙÛÜŒ]]

local function cs2str (c)
  return lpeg.match(lpeg.Cs((c + lpeg.P(1)/"")^0), allchar)
end

--table.insert(a,specialFrench)
locale.specialFrench = specialFrench
--print("type de a :g , type(a))
--a:insert(specialFrench)
local function eqcharset (c1, c2)
  assert(cs2str(c1) == cs2str(c2))
end

--print(1,cs2str(lpeg.P(1)))

for k,v in pairs(locale) do 
  print(k,cs2str(v))
end

