do
  local lpeg = require"lpeg"

  local anyChar = lpeg.P(1)
  local word = (lpeg.R("az") + lpeg.R("AZ"))^1

  do 
    function lazy(p,q)
      local q=q or lpeg.P(0)
      return lpeg.P{ q + p * lpeg.V(1) }
    end
    function verifier(s,i,capture)
      local res = tab[capture]
      return res and true, res
    end
    function split(corde,sep)
      local sep = lpeg.P(sep) 
      local pat = lpeg.Ct((lpeg.C(lazy(anyChar,#sep)) * sep + lpeg.C(anyChar^1) * -1)^0)
      return pat:match(corde)
    end
    function splitLines(corde)
      return split(corde,"\n")
    end
  end

  do
    local theEnvi = lpeg.Cb("envName")
    local beg = lpeg.P
    {
      [[\begin{]] * lpeg.Cg(word,"envName") * theEnvi * lpeg.Cmt(theEnvi, verifier) * [[}]]
    }
    local patDeb = lazy(anyChar-[[%]],lpeg.Cp() * beg) * lpeg.Cp() 

    tab = {verba= function (corde) cedric=corde end}
    function faireEnvi(envi,fonction)
      --loadstring("tab['" .. envi .. "']=" .. fonction)()
      tab[envi] = fonction
    end

    -- captures commented newline if no text, captures subject otherwise
    local commentIfWhite = lpeg.P(" ")^0 * ([[%]] * anyChar^0 )^-1 * lpeg.Cc("%") * -1 + lpeg.C(lpeg.P(1)^0) 

    local luaVerbatimIsRecording=false
    function readbuf(ligne)
      if not luaVerbatimIsRecording then
        local d,envi,f,nd = patDeb:match(ligne)
        luaVerbatimCalledFunction = f
        if not d then
          return nil
        else -- no match for begin{envi}
          local beforeEnvi, insideEnvi = ligne:sub(1,d-1), ligne:sub(nd)
          luaVerbatimBody, luaVerbatimIsRecording = "", true
          patEnd = lazy(anyChar,lpeg.Cp() * ([[\end{]] .. envi .. [[}]])) * lpeg.Cp() 
          return beforeEnvi .. readbuf(insideEnvi)
        end
      else -- not luaVerbatimIsRecording
        local d,nd = patEnd:match(ligne)
        if not d then
          luaVerbatimBody = luaVerbatimBody .. ligne .. "\n"
          return "%"
        else -- no match for end{envi}
          local insideEnvi, afterEnvi = ligne:sub(1,d-1), ligne:sub(nd)
          luaVerbatimBody, luaVerbatimIsRecording = luaVerbatimBody .. insideEnvi .. "\n", false
          return [[\directlua{luaVerbatimCalledFunction(luaVerbatimBody)}]] .. commentIfWhite:match(afterEnvi) 
        end
      end
    end

  end
end
